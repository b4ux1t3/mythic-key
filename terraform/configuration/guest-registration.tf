resource "keycloak_authentication_flow" "guest_reg" {
  alias       = "Alt registration"
  description = "registration flow"
  provider_id = "basic-flow"
  realm_id    = keycloak_realm.MythicTable.id
}

resource "keycloak_authentication_subflow" "guest_reg_form" {
  alias             = "Alt registration registration form"
  provider_id       = "form-flow"
  description       = "registration form"
  parent_flow_alias = keycloak_authentication_flow.guest_reg.alias
  realm_id          = keycloak_realm.MythicTable.id
  requirement       = "REQUIRED"
  authenticator     = "registration-page-form"
}

resource "keycloak_authentication_execution" "guest" {
  authenticator     = "registration-noinput-user-creation"
  parent_flow_alias = keycloak_authentication_subflow.guest_reg_form.alias
  realm_id          = keycloak_realm.MythicTable.id
  requirement       = "REQUIRED"
}

resource "keycloak_authentication_execution" "guest_validation" {
  authenticator     = "registration-passwordless-action"
  parent_flow_alias = keycloak_authentication_subflow.guest_reg_form.alias
  realm_id          = keycloak_realm.MythicTable.id
  requirement       = "REQUIRED"
  depends_on = [
    keycloak_authentication_execution.guest
  ]

}

resource "keycloak_authentication_execution" "guest_recaptcha" {
  authenticator     = "registration-recaptcha-action"
  parent_flow_alias = keycloak_authentication_subflow.guest_reg_form.alias
  realm_id          = keycloak_realm.MythicTable.id
  requirement       = "DISABLED"
  depends_on = [
    keycloak_authentication_execution.guest_validation
  ]
}

resource "keycloak_authentication_bindings" "bindings" {
  realm_id          = keycloak_realm.MythicTable.id
  registration_flow = keycloak_authentication_flow.guest_reg.alias
}