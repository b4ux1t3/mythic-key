<#import "template.ftl" as layout>
<@layout.registrationLayout doubleColumn=false; section>
    <#if section = "header">
        ${msg("registerTitle",(realm.name!''))}
    <#elseif section = "form">
        <div class="registration">
            <#-- App-initiated actions should not see warning messages about the need to complete the action -->
            <#-- during login.                                                                               -->
            <#if message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
                <div class="alert alert-${message.type}">
                    <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                    <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                    <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                    <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                    <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                </div>
            </#if>

            <form id="kc-register-form" action="${url.registrationAction}" method="post">
                <input type="hidden" name="firstName" value="Mythic" />
                <input type="hidden" name="lastName" value="Player" />
                <#if guest??>
                    <input type="hidden" name="guest" value="true"/>
                    <script>
                        document.getElementById("kc-register-form").submit();
                    </script>
                <#else>
                    <div>
                        <label for="email">${msg("email")}</label>
                        <input tabindex="1" id="email" name="email" value="${(register.formData.email!'')}"  type="text" autofocus autocomplete="off" />
                    </div>

                    <div>
                        <label for="password">${msg("password")}</label>
                        <input tabindex="2" id="password" name="password" type="password" autocomplete="off" />
                    </div>

                    <div>
                        <label for="password-confirm">${msg("passwordConfirm")}</label>
                        <input tabindex="3" id="password-confirm" name="password-confirm" type="password" autocomplete="off" />
                    </div>

                    <div class="login-actions">
                        <div class="forgot-password"><a href="${url.loginUrl}">${msg("backToLogin")?no_esc}</a></div>
                    </div>

                    <div id="form-buttons">
                        <button tabindex="4" name="login" id="kc-login" type="submit">${msg("doRegister")}</button>
                    </div>
                </#if>
            </form>
        </div>
    </#if>
</@layout.registrationLayout>