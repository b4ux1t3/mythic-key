# Getting KeyCloak to work with MythicTable

## What is KeyCloak?
Keycloak is an Open Source Identity and Access Management system, which provides JWT tokens to applications to authenitcate users.  KeyCloak supports standard username/password login as well as OAuth2 flows.

## Running MythicTable with KeyCloak
KeyCloak is a drop-in replacement for the current Auth service

### Starting KeyCloak
Run `docker build -t {container} {location}` within the project folder, then `docker run -d --rm -p 5002:8080 --name mythic-key mythic-key:local`.

```
$ cd mythic-key
$ docker build -t mythic-key:local .
$ docker run -d --rm -p 5002:8080 --name mythic-key mythic-key:local
```
Notice the `.` in the docker build command. That's assuming you're running the command from the project folder

### Backend
To run the backend so it will authenticate with KeyCloak, use `dotnet run --launch-profile local-keycloak`

```
$ cd server/src/MythicTable
$ dotnet run --launch-profile local-keycloak
or
$ dotnet run --project .\server\src\MythicTable\MythicTable.csproj --launch-profile local-keycloak
```

### Frontend
To run the frontend so it will authenticate using KeyCloak, run `npm run serve -- --mode=local-keycloak`

```
$ cd html
$ npm run serve -- --mode=local-keycloak
```

## Extending KeyCloak
Keyclaok allows you to create plugins to extend behavior via their SPI. You will need to generate a jar file containing classes implementing and overriding behavior. See: [Keycloak examples](https://github.com/keycloak/keycloak/tree/master/examples). This requires Maven and Java.

### Maven Parent
For our use, we created a POM parent to be used when creating plugins for authentication extension. If we decide to add more plugins, more parent POMs might need to be added, and some folder restructuring to organize it.

The current parent can be found under the plugin folder.

### Build plugin and deploy
The docker build instruction for starting keycloak should take care of it all. But, if you need to do some nmanual
To build, navigate to the desired plugin folder and run the command `mvn clean install`. You will find the jar within the `target` folder in the location your ran the command.

To deploy it you will need to copy the jar file into the running keycloak application's `/standalone/deployments/` folder. The full path in docker container would be `{container}/opt/jboss/keycloak/standalone/deployments/` where `{container}` is the specific container running keycloak.

The dockerfile should be configured to do this. But, you can manually add a new extension by running `docker cp ./plugin/{plugin-folder}/target/{plugin-name-jar-file} {container}:/opt/jboss/keycloak/standalone/deployments/`. And, watch for logs of deployment. You will now be able to use the extension in the keycloak config.

Mmanually (while container is running during dev):

```
$ cd plugin/custom-registration-form-action
$ mvn clean install
$ docker cp ./target/custom-registration-form-action.jar mythic-key:/opt/jboss/keycloak/standalone/deployments/
```

if you want to uninstall the deployed plugin you have to delete the `.jar.deployed` file first. Then the corresponding `.jar` file. This will allow you to re-upload and re-deploy by copying the file again.

```
$ docker container exec mythic-key rm /opt/jboss/keycloak/standalone/deployments/custom-registration-form-action.jar.deployed
$ docker container exec mythic-key rm /opt/jboss/keycloak/standalone/deployments/custom-registration-form-action.jar
```