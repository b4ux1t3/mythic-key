FROM maven:3.6.0-jdk-11-slim AS build
ADD ./plugin/ /home/plugin/
WORKDIR /home/plugin/custom-registration-form-action/
RUN mvn clean install

FROM quay.io/keycloak/keycloak:12.0.1

ENV PROXY_ADDRESS_FORWARDING true
ENV KEYCLOAK_USER admin
ENV KEYCLOAK_PASSWORD admin
ENV KEYCLOAK_IMPORT /tmp/realm.json

COPY --from=build /home/plugin/custom-registration-form-action/target/custom-registration-form-action.jar /opt/jboss/keycloak/standalone/deployments/custom-registration-form-action.jar
COPY ./realm.json /tmp/realm.json
COPY ./MythicTable /opt/jboss/keycloak/themes/MythicTable
